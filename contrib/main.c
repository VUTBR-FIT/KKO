/*
 * Autor: Lukas Cerny (xcerny63)
 * Datum: 6.5.2018
 * Soubor: main.c
 * Komentar: Parsovani vstupu a provedeni operaci na zaklade vstupu
 */
#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <stdbool.h>
#include "ahed.h"

/**
 * Vypsani napovedy na STDOUT
 */
void printHelp() {
    printf("%s\n", "ahed [-i FILE] [-o FILE] [-l FILE] [-h] [-c|-x]");
    printf("%s\n", "Parameters:");
    printf("\t%s\n", "-i <FILE> Input file");
    printf("\t%s\n", "-o <FILE> Output file");
    printf("\t%s\n", "-l <FILE> Log file");
    printf("\t%s\n", "-c Enable encoding mode");
    printf("\t%s\n", "-h Print this help");
    printf("\t%s\n", "-x Enable decoding mode");
}

int main(int argc, char **argv) {
    int c, errorParam = 0, errorCode = EXIT_SUCCESS;
    char *inputFile = NULL, *outputFile = NULL, *logFile = NULL;
    bool flagDecode = false, flagEncode = false, flagHelp = false;

    while ((c = getopt(argc, argv, "i:o:l:cxh")) != -1) {
        switch (c) {
            case 'i':
                if (inputFile != NULL)
                    errorParam = c;
                inputFile = optarg;
                break;
            case 'o':
                if (outputFile != NULL)
                    errorParam = c;
                outputFile = optarg;
                break;
            case 'l':
                if (logFile != NULL)
                    errorParam = c;
                logFile = optarg;
                break;
            case 'c':
                if (flagEncode != false)
                    errorParam = c;
                flagEncode = true;
                break;
            case 'x':
                if (flagDecode != false)
                    errorParam = c;
                flagDecode = true;
                break;
            case 'h':
                if (flagHelp != false)
                    errorParam = c;
                flagHelp = true;
                break;
        }
        if (errorParam != 0)
            break;
    }
    if (errorParam) {
        fprintf(stderr, "Duplicate params %c.", errorParam);
        errorCode = EXIT_FAILURE;
    } else if (flagEncode == true && flagDecode == true) {
        printError("Bad combination of parameters 'c' and 'x'.");
        errorCode = EXIT_FAILURE;
    } else if (flagHelp) {
        printHelp();
    } else {
        FILE *input = NULL, *output = NULL, *log = NULL;
        int errorno = 0;
        tAHED ahed;

        if (inputFile == NULL)
            input = stdin;
        else
            input = fopen(inputFile, "r");
        if (input == NULL) {
            printError("Input file can not be opened for reading.");
            exit(EXIT_FAILURE);
        }

        if (outputFile == NULL)
            output = stdout;
        else
            output = fopen(outputFile, "w");
        if (output == NULL) {
            printError("Output file can not be opened for writing.");
            if (inputFile != NULL)
                fclose(input);
            exit(EXIT_FAILURE);
        }
        if (logFile != NULL) {
            log = fopen(logFile, "w");
            if (log == NULL) {
                printError("Output file can not be opened for writing.");
                if (inputFile != NULL)
                    fclose(input);
                if (outputFile != NULL)
                    fclose(output);
                exit(EXIT_FAILURE);
            }
        }
        
        
        DEBUG_LOG("MAIN", "BEFORE decode/encode");
        if (flagDecode) {
            errorCode = AHEDDecoding(&ahed, input, output);
        } else if (flagEncode) {
            errorCode = AHEDEncoding(&ahed, input, output);
        }
        DEBUG_LOG("MAIN", "AFTER decode/encode");
        
        if (errorCode != AHEDOK) {
            printError("Error in decoding/encoding.");
        } else if (logFile != NULL) {
            fprintf(log, "login=%s\n", "xcerny63");
            fprintf(log, "uncodedSize=%ld\n", ahed.uncodedSize);
            fprintf(log, "codedSize=%ld\n", ahed.codedSize);
        }
        if (inputFile != NULL)
            fclose(input);
        if (outputFile != NULL)
            fclose(output);
        if (logFile != NULL)
            fclose(log);
    }

    exit(errorCode);
}
