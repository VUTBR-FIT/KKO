/*
 * Autor: Lukáš Černý (xcerny63)
 * Datum: 12.2.2018
 * Soubor: ahead.c
 * Komentar:
 */

#include "ahed.h"
#include <string.h>

void nodePrint(tNode *node) {
    if (node != NULL)
        fprintf(stdout, "Node: value/order/weight (%d/%d/%d)\n", node->value, node->order, node->weight);
}

void printError(char* message) {
    fprintf(stderr, "[ERROR]\t %s\n", message);
}

void printCharAsBinary(char c) {
    for (int i = 7; i >= 0; --i) {
        fprintf(stderr, "%d", (c & (1 << i)) ? 1 : 0);
    }
    fprintf(stderr, "%s", "\n");
}

void printBinaryData(FILE *output) {
#ifdef DEBUG_ENABLED
    printCharAsBinary(outputChar);
#endif
    fprintf(output, "%c", outputChar);
    for (int i = 0; i < BITS_COUNT; i++) {
        outputChar &= (0 << (BITS_COUNT - (i + 1)));
    }
    outputCharPos = 0;
}

int printSymbol(char c, FILE *output) {
    int countOfSymbols = 0;
    for (int i = 0; i < BITS_COUNT; i++) {
        int bit = c >> (BITS_COUNT - (i + 1));
        outputChar |= (bit << (BITS_COUNT - (outputCharPos + 1)));
        outputCharPos++;
        if (outputCharPos == BITS_COUNT) {
            printBinaryData(output);
            countOfSymbols++;
        }
    }
    return countOfSymbols;
}

int printEncodedSymbol(tNode *node, FILE *outputFile) {
    tNode *current = node;
    int codePath[NODE_MAX_COUNT];
    int length = 0, bit = 0;
    int printedSymbols = 0;
    DEBUG_LOG("PRINT_ENCODED", "START");
    while (current->parent != NULL) {
        DEBUG_LOG("PRINT_ENCODED", "WHILE");
        bit = 0;
        if (current->parent->right == current)
            bit = 1;
        codePath[length] = bit;
        length++;
        current = current->parent;
    }
    
    for (int i = length - 1; i >= 0; i--) {
        bit = codePath[i];
        outputChar |= (bit << (BITS_COUNT - (outputCharPos + 1)));
        outputCharPos++;
        if (outputCharPos == BITS_COUNT) {
            printBinaryData(outputFile);
            printedSymbols++;
            outputCharPos = 0;
        }
    }
    
    return printedSymbols;
}

tTree *treeInit() {
    tTree *tree = (tTree *) malloc(sizeof (tTree));
    if (tree == NULL)
        return NULL;

    tree->root = nodeInit();
    if (tree->root == NULL) {
        free(tree);
        return NULL;
    }
    tree->root->order = NODE_MAX_COUNT;
    tree->root->value = NYT; // Prvni ve stromu je NYT

    tree->nodes = (tNode **) malloc(sizeof (tNode *) * NODE_MAX_COUNT);
    if ((tree->nodes) == NULL) {
        nodeDelete(tree->root);
        free(tree);
        return NULL;
    }
    tree->nodes[NYT] = tree->root;
    return tree;
}

void treeDelete(tTree * tree) {
    if (tree != NULL) {
        nodeDelete(tree->root);
        free(tree->nodes);
        free(tree);
    }
}

bool treeInsert(tTree *tree, unsigned int c) {
    DEBUG_LOG("TREE INSERT", "START");
    tNode *left = tree->nodes[NYT];
    tNode *parent = nodeInit();
    if (parent == NULL)
        return false;
    tNode *right = nodeInit();
    if (right == NULL) {
        free(left);
        return false;
    }
    
    DEBUG_LOG("TREE INSERT", "INIT OK");
    parent->left = left;
    parent->right = right;
    parent->parent = left->parent;
    if (parent->parent != NULL)
        parent->parent->left = parent;
    parent->value = SYMBOL_EMPTY;
    parent->order = left->order;
    parent->weight = left->weight;

    DEBUG_LOG("TREE INSERT", "PARENT SET OK");
    left->parent = right->parent = parent;
    left->order -= 2;

    right->value = c;
    right->order = parent->order - 1;

    DEBUG_LOG("TREE INSERT", "LEFT & RIGHT SET OK");
    tree->nodes[right->value] = right;
    tree->nodes[SYMBOL_COUNT + parent->order / 2] = parent;
    if (parent->parent == NULL)
        tree->root = parent;
    
    DEBUG_LOG("TREE INSERT", "END");
    return true;
}

void treeAdaptUpdate(tTree *tree, unsigned int c) {
    DEBUG_LOG("ADAPT_TREE", "START");
    if (tree == NULL)
        return;

    tNode *updated = tree->nodes[c];
    tNode *parent, *founded;
    while (updated != NULL) {
        founded = updated;
        for (int i = 0; i < NODE_MAX_COUNT; i++) {
            if (tree->nodes[i] != NULL // Hledam nejaky symbol, ktery se uz precetl
                    && tree->nodes[i]->weight == updated->weight // Hledam nekoho kdo ma pred zvysenim vahy stejnou vahu
                    && founded->order < tree->nodes[i]->order) { // Hledam nekoho kdo je blize ke koreni
                founded = tree->nodes[i];
            }
        }

        // prohozeni dvou nalezenych
        if (founded != updated && updated->parent != founded && updated->parent != NULL) {
            parent = updated->parent;
            if (parent->right == updated) parent->right = founded;
            else parent->left = founded;

            parent = founded->parent;
            if (parent->right == founded) parent->right = updated;
            else parent->left = updated;

            unsigned int order = founded->order;
            founded->parent = updated->parent;
            founded->order = updated->order;
            updated->parent = parent;
            updated->order = order;
        }

        updated->weight++;
        if (tree->root == updated) // pokud upravovany je koren, tak skonci
            break;
        else // jinak jdi ke korenu
            updated = updated->parent;
    }
}

tNode *nodeInit() {
    tNode *temp = (tNode *) malloc(sizeof (tNode));
    if (temp == NULL)
        return NULL;
    temp->parent = temp->right = temp->left = NULL;
    temp->order = temp->weight = 0;
    temp->value = SYMBOL_EMPTY; // Pri inicializaci vlozime prazdny znak
    return temp;
}

void nodeDelete(tNode *node) {
    if (node != NULL) {
        nodeDelete(node->left);
        nodeDelete(node->right);
        free(node);
    }
}

int AHEDEncoding(tAHED *ahed, FILE *inputFile, FILE *outputFile) {
    ahed->uncodedSize = ahed->codedSize = outputCharPos = 0;
    tTree *tree = treeInit();
    if (tree == NULL) {
        printError("Init main TREE failed");
        return AHEDFail;
    };
    DEBUG_LOG("ENCODING", "AFTER INIT");
    unsigned int c = 0;
    while (fread(&c, 1, 1, inputFile) != 0) {
        ahed->uncodedSize++;
#ifdef DEBUG_ENABLED
        DEBUG_LOG("ENCODING", "CYCLE");
        fprintf(stderr, "READED %u\n", c);
        printCharAsBinary(c);
#endif
        if (tree->nodes[c] == NULL) { // Prijat novy symbol
            DEBUG_LOG("ENCODING", "A-CYCLE");
            // Dostal jsem novy symbol, tak musím vypsat zarazku
            ahed->codedSize += printEncodedSymbol(tree->nodes[NYT], outputFile);
            ahed->codedSize += printSymbol(c, outputFile);
            if (!treeInsert(tree, c)) {
                treeDelete(tree);
                printError("Inser FAILED");
                return AHEDFail;
            }
        } else
            ahed->codedSize += printEncodedSymbol(tree->nodes[c], outputFile);
        treeAdaptUpdate(tree, c);
    }
    ahed->codedSize += printEncodedSymbol(tree->nodes[NYT], outputFile);

    if (outputCharPos != 0) {
        printBinaryData(outputFile);
        ahed->codedSize++;
    }

    DEBUG_LOG("ENCODING", "END");
    treeDelete(tree);
    return AHEDOK;
}

int AHEDDecoding(tAHED *ahed, FILE *inputFile, FILE *outputFile) {
    ahed->uncodedSize = ahed->codedSize = outputCharPos = 0;
    tTree *tree = treeInit();
    if (tree == NULL) {
        printError("Init main TREE failed");
        return AHEDFail;
    };
    DEBUG_LOG("DECODING", "AFTER INIT");
    unsigned int c = 0;
    int bit = 0;
    int nyt = 1; // Pred prvnim symbolem je vlozeny NYT, ale jeho delka je nulova 
    tNode *current = tree->root;
    while (fread(&c, 1, 1, inputFile) != 0) {
#ifdef DEBUG_ENABLED
        DEBUG_LOG("DECODING", "CYCLE");
        fprintf(stderr, "READED %u\n", c);
        printCharAsBinary(c);
#endif
        for (int i = 0; i < BITS_COUNT; i++) {
            bit = c >> (BITS_COUNT - (i+1)) & 1;
#ifdef DEBUG_ENABLED
        DEBUG_LOG("DECODING", "CYCLE-BIT");
        fprintf(stderr, "BIT %d\n", (int) bit);
#endif

            if (nyt == 1) {// Musim nacist 8b a ty pak vypsat a vlozit do stromu
                DEBUG_LOG("DECODING", "NYT ON");
                outputChar |= (bit << (BITS_COUNT - (outputCharPos + 1)));
                outputCharPos++;

                if (outputCharPos == BITS_COUNT) {
                    DEBUG_LOG("DECODING", "FULL CHAR");
                    if (!treeInsert(tree, outputChar)) {
                        treeDelete(tree);
                        printError("Inser FAILED");
                        return AHEDFail;
                    }
                    DEBUG_LOG("DECODING", "UPDATE TREE");
                    treeAdaptUpdate(tree, outputChar);
                    printBinaryData(outputFile);
                    nyt = 0;
                    current = tree->root;
                    ahed->uncodedSize++;
                }
            } else {// Nactena data musim hledat ve strome a nalezeny vysledek pak vypsat
                if (bit == 1 || bit == -1)
                    current = current->right;
                else
                    current = current->left;

                if (current == NULL) {
                    printError("Loaded data can not be founded in tree.");
                    treeDelete(tree);
                    return AHEDFail;
                } else if (current->value == NYT) {
                    DEBUG_LOG("DECODING", "NYT FOUNDED");
                    nyt = 1;
                } else if (current->value < NYT) {
                    DEBUG_LOG("DECODING", "SYMBOL FOUNDED");
                    ahed->uncodedSize++;
                    printSymbol(current->value, outputFile);
                    treeAdaptUpdate(tree, current->value);
                    current = tree->root;
                }
            }

        }
        ahed->codedSize++;
    }

    DEBUG_LOG("DECODING", "END");
    treeDelete(tree);
    return AHEDOK;
}
