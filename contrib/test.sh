#!/bin/sh
# 
# File:   test.sh
# Author: user
#
# Created on May 3, 2018, 11:46:42 AM
#
program="./ahed"
if [ $# -eq 1 ]; then
    ${program} -c -i $1 -o temp.txt
    ${program} -x -i temp.txt -o temp2.txt
    diff $1 temp2.txt
fi;