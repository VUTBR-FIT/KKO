/*
 * Autor: Lukáš Černý (xcerny63)
 * Datum: 6.5.2018
 * Soubor: ahead.h
 * Komentar: Deklarace funkci, debugovacich maker a struktur
 */ 

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#ifndef __KKO_AHED_H__
#define __KKO_AHED_H__

#define AHEDOK 0
#define AHEDFail -1

#ifdef DEBUG_ENABLED
#define DEBUG_LOG(...) fprintf(stderr, "[%s]\t %s\n", __VA_ARGS__);
#else
#define DEBUG_LOG(...) while (false);
#endif

#define BITS_COUNT 8 // Symboly (0-255) lze binarne zapsat na 8 bitech
#define SYMBOL_COUNT (256 + 1) // Pocet symbolu + NYT
#define NODE_MAX_COUNT (2 * SYMBOL_COUNT) // Uzly pro symboly + prazdne uzly
#define NYT (SYMBOL_COUNT - 1) // Not-Yet-Trasnmitted
#define SYMBOL_EMPTY SYMBOL_COUNT // Uzly ktere neobsahuji hodnotu

// Datovy typ zaznamu o (de)kodovani
typedef struct{
	/* velikost nekodovaneho retezce */
	int64_t uncodedSize;
	/* velikost kodovaneho retezce */
	int64_t codedSize;
} tAHED;

// Struktura jednoho uzlu
typedef struct node tNode;
struct node {
    unsigned int order;
    unsigned int value;
    unsigned int weight;
    
    tNode *left;
    tNode *right            ;
    tNode *parent;
};
// Strukutra pro strom
typedef struct tree {
    tNode *root;
    tNode **nodes; 
} tTree;

unsigned int outputChar;    // Znak k zapsani symbolu
int outputCharPos;          // Pozice zmenenych bitu

/**
 * Vypise obsah predanebo uzlu
 * @param node  Uzel
 */
void nodePrint(tNode *node);

/**
 * Napise chybovou zpravu na stderr
 * @param message
 */
void printError(char* message);

/**
 * Vypise binarni kod symbolu na stdout
 * @param c     Znak k ziskani binarniho kodu
 */
void printCharAsBinary(char c);

/**
 * Vypise zapisovany znak do souboru
 * @param output Vystupni soubor
 */
void printBinaryData(FILE *output);

/**
 * Zapise symbol do souboru
 * @param c         Znak k vypsani
 * @param output    Vystupni soubor
 * @return          Pocet zapsany symbolu
 */
int printSymbol(char c, FILE *output);

/**
 * Zapise cestu od korene k danemu uzlu do zapisovaneho symbolu
 * @param node          Uzel, ke kteremu se ma vypsat cesta
 * @param outputFile    Vystupni soubor
 * @return              Pocet vypsanych znaku do vystupniho souboru
 */
int printEncodedSymbol(tNode *node, FILE *outputFile);

/**
 * Inicializuje strukturu stromu
 * @return Vytvoreny strom nebo NULL
 */
tTree *treeInit();

/**
 * Smaze celou strukturu stromu
 * @param tree  Strom k smazani
 */
void treeDelete(tTree * tree);

/**
 * Vlozi do stromu novy uzel, do ktereho vlozi nove projaty symbol
 * @param tree  Strom
 * @param c     Vkladany symbol
 * @return      Uspech operace
 */
bool treeInsert(tTree *tree, unsigned int c);

/**
 * Upravi srom podle noveho vyskytu uzlu
 * @param tree  Ulozeny strom
 * @param c     Vloženy symbol 
 */
void treeAdaptUpdate(tTree *tree, unsigned int c);

/**
 * Inicializuje novy uzel
 * @return Novy uzel nebo NULL
 */
tNode *nodeInit();

/**
 * Smaze uzel a jeho syny
 * @param node Uzel k smazani
 */
void nodeDelete(tNode *node);

/* Nazev:
 *   AHEDEncoding
 * Cinnost:
 *   Funkce koduje vstupni soubor do vystupniho souboru a porizuje zaznam o kodovani.
 * Parametry:
 *   ahed - zaznam o kodovani
 *   inputFile - vstupni soubor (nekodovany)
 *   outputFile - vystupni soubor (kodovany)
 * Navratova hodnota: 
 *    0 - kodovani probehlo v poradku
 *    -1 - pri kodovani nastala chyba
 */
int AHEDEncoding(tAHED *ahed, FILE *inputFile, FILE *outputFile);

/* Nazev:
 *   AHEDDecoding
 * Cinnost:
 *   Funkce dekoduje vstupni soubor do vystupniho souboru a porizuje zaznam o dekodovani.
 * Parametry:
 *   ahed - zaznam o dekodovani
 *   inputFile - vstupni soubor (kodovany)
 *   outputFile - vystupni soubor (nekodovany)
 * Navratova hodnota: 
 *    0 - dekodovani probehlo v poradku
 *    -1 - pri dekodovani nastala chyba
 */
int AHEDDecoding(tAHED *ahed, FILE *inputFile, FILE *outputFile);

#endif

